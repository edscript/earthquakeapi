package com.springboot.interview.earthquake.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class EarthquakeInfo {
    @JsonProperty("features")
    private List<Feature> features;


    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }
}
