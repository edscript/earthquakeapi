package com.springboot.interview.earthquake.domain;

public class Properties {

    private Float mag;
    private String place;
    private Long time;
    private Long updated;
    private Integer tz;
    private String url;
    private String detail;
    private Integer felt;
    private Float cdi;
    private Float mmi;
    private String alert;
    private String status;
    private Integer tsunami;
    private Integer sig;
    private String net;
    private String code;
    private String ids;
    private String sources;
    private String types;
    private Integer nst;
    private Float dmin;
    private Float rms;
    private Float gap;
    private String maqType;
    private Integer String;


    public Float getMag() {
        return mag;
    }

    public void setMag(Float mag) {
        this.mag = mag;
    }

    public java.lang.String getPlace() {
        return place;
    }

    public void setPlace(java.lang.String place) {
        this.place = place;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Integer getTz() {
        return tz;
    }

    public void setTz(Integer tz) {
        this.tz = tz;
    }

    public java.lang.String getUrl() {
        return url;
    }

    public void setUrl(java.lang.String url) {
        this.url = url;
    }

    public java.lang.String getDetail() {
        return detail;
    }

    public void setDetail(java.lang.String detail) {
        this.detail = detail;
    }

    public Integer getFelt() {
        return felt;
    }

    public void setFelt(Integer felt) {
        this.felt = felt;
    }

    public Float getCdi() {
        return cdi;
    }

    public void setCdi(Float cdi) {
        this.cdi = cdi;
    }

    public Float getMmi() {
        return mmi;
    }

    public void setMmi(Float mmi) {
        this.mmi = mmi;
    }

    public java.lang.String getAlert() {
        return alert;
    }

    public void setAlert(java.lang.String alert) {
        this.alert = alert;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public Integer getTsunami() {
        return tsunami;
    }

    public void setTsunami(Integer tsunami) {
        this.tsunami = tsunami;
    }

    public Integer getSig() {
        return sig;
    }

    public void setSig(Integer sig) {
        this.sig = sig;
    }

    public java.lang.String getNet() {
        return net;
    }

    public void setNet(java.lang.String net) {
        this.net = net;
    }

    public java.lang.String getCode() {
        return code;
    }

    public void setCode(java.lang.String code) {
        this.code = code;
    }

    public java.lang.String getIds() {
        return ids;
    }

    public void setIds(java.lang.String ids) {
        this.ids = ids;
    }

    public java.lang.String getSources() {
        return sources;
    }

    public void setSources(java.lang.String sources) {
        this.sources = sources;
    }

    public java.lang.String getTypes() {
        return types;
    }

    public void setTypes(java.lang.String types) {
        this.types = types;
    }

    public Integer getNst() {
        return nst;
    }

    public void setNst(Integer nst) {
        this.nst = nst;
    }

    public Float getDmin() {
        return dmin;
    }

    public void setDmin(Float dmin) {
        this.dmin = dmin;
    }

    public Float getRms() {
        return rms;
    }

    public void setRms(Float rms) {
        this.rms = rms;
    }

    public Float getGap() {
        return gap;
    }

    public void setGap(Float gap) {
        this.gap = gap;
    }

    public java.lang.String getMaqType() {
        return maqType;
    }

    public void setMaqType(java.lang.String maqType) {
        this.maqType = maqType;
    }

    public Integer getString() {
        return String;
    }

    public void setString(Integer string) {
        String = string;
    }
}
