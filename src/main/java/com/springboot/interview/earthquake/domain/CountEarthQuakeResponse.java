package com.springboot.interview.earthquake.domain;

public class CountEarthQuakeResponse {
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
