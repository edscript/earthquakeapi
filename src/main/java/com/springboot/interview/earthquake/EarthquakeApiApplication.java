package com.springboot.interview.earthquake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.springboot.*"})
public class EarthquakeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EarthquakeApiApplication.class, args);
	}

}
