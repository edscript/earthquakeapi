package com.springboot.interview.earthquake.helper;

import org.springframework.web.util.UriComponentsBuilder;

public class EarthquakeApiHelper {

    //dates
    public static final String START_DATE_NAME = "starttime";
    public static final String END_DATE_NAME = "endtime";

    //magnitude
    public static final String MIN_MAGNITUDE_NAME = "minmagnitude";
    public static final String MAX_MAGNITUDE_NAME = "maxmagnitude";

    /**
     * Helper add Dates
     * @param url Dynamic dates received
     * @param starttime Start date
     * @param enddate End date
     * @return Url with the params added
     */
    public static String addUrlDateParams(String url, String starttime, String enddate){
        if(url==null) url = "";
        return UriComponentsBuilder.fromUriString(url).queryParam(START_DATE_NAME,starttime).
                queryParam(END_DATE_NAME,enddate).build().toString();
    }

    /**
     * Helper add Magnitudes in the URL
     *
     * @param url Dynamic dates received
     * @param minmagnitude Min magnitude
     * @param maxmagnitude Min magnitude
     * @return Url with the magnitude params added
     */
    public static String addUrlMagnitudeParams(String url, String minmagnitude, String maxmagnitude){
        if(url==null) url="";
        return UriComponentsBuilder.fromUriString(url).queryParam(MIN_MAGNITUDE_NAME,minmagnitude).
                queryParam(MAX_MAGNITUDE_NAME,maxmagnitude).build().toString();
    }
}
