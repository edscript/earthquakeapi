package com.springboot.interview.earthquake.controller;


import com.springboot.interview.earthquake.domain.CountEarthQuakeResponse;
import com.springboot.interview.earthquake.domain.Feature;
import com.springboot.interview.earthquake.service.EarthquakeApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@Validated
@RequestMapping("/earthquakeAPI/v1/")
public class EarthquakeApiController {

    @Autowired
    private EarthquakeApiService earthquakeApiService;

    /**
     * Endpoint to process the earthquakes by the given dates
     * @param startDate First date in the range to be search
     * @param endDate Final date in the range to be search
     * @throws ResponseStatusException if there's any error processing the request in the service layer
     * @return List of earthquakes
     */
    @GetMapping(value="/datesrange")
    public ResponseEntity retrieveByTwoDatesRange(@PathParam("startDate") @DateTimeFormat(pattern="yyyy-mm-dd") String startDate,
                                             @PathParam("endDate") @DateTimeFormat(pattern="yyyy-mm-dd") String endDate){

        List<Feature> features = null;
        try {
            features = earthquakeApiService.retrieveEarthquakesByDates(startDate, endDate);
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
        return new ResponseEntity<List<Feature>>(features, HttpStatus.OK);
    }

    /**
     * Endpoint to process the earthquakes by the given dates
     * @param startDate1 First date in the range of the first group to be search
     * @param endDate1 Final date in the range of the first group to be search
     * @param startDate2 First date in the range of the second group to be search
     * @param endDate2 First date in the range of the second group to be search
     * @throws ResponseStatusException if there's any error processing the request in the service layer
     * @return List of earthquakes
     */

    @GetMapping(value="/multidatesrange")
    public ResponseEntity retrieveByFourDatesRange(@PathParam("startDate1") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String startDate1,
                                            @PathParam("endDate1") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String endDate1,
                                            @PathParam("startDate2") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String startDate2,
                                            @PathParam("endDate2") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String endDate2) {

        List<Feature> features = null;
        try{
            features = earthquakeApiService.retrieveEarthquakesByDates(startDate1, endDate1, startDate2, endDate2);
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
        return new ResponseEntity<List<Feature>>(features, HttpStatus.OK);

    }

    /**
     * Endpoint to process the earthquakes by the given magnitudes
     * @param minMagnitude Min magnitude
     * @param maxMagnitude Mx magnitude
     * @throws ResponseStatusException if there's any error processing the request in the service layer
     * @return List of earthquakes
     */

    @GetMapping(value="/magnitudes")
    public ResponseEntity retrieveByMagnitudes(@PathParam("minMagnitude") @NumberFormat @Valid String minMagnitude,
                                               @PathParam("maxMagnitude") @NumberFormat @Valid String maxMagnitude){

        List<Feature> features = null;
        try {
            features = earthquakeApiService.retrieveEarthquakesByMagnitudes(minMagnitude, maxMagnitude);
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        return new ResponseEntity<List<Feature>>(features, HttpStatus.OK);

    }
    /**
     * Endpoint to process the earthquakes by the given country, since the USGS doesn't have any direct way
     * to filter the country, a default search is performed in USGS API and then is filtered by the layer service
     * @param country country to be filtered
     * @throws ResponseStatusException if there's any error processing the request in the service layer
     * @return List of earthquakes
     */
    @GetMapping(value="/country")
    public ResponseEntity retrieveByCountry(@PathParam("country") @Pattern(regexp = "^[A-Za-z]+$")
                                                   @Valid String country){

        List<Feature> features = null;
        try {
            features = earthquakeApiService.retrieveEarthquakesByCountry(country);
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        return new ResponseEntity<List<Feature>>(features, HttpStatus.OK);

    }

    /**
     * Endpoint to process the earthquakes by the given countries and dates
     * @param country1 country of the first group to be filtered
     * @param country2 country of the first group to be filtered
     * @param startDate1 First date in the range of the first group to be search
     * @param endDate1 Final date in the range of the first group to be search
     * @param startDate2 First date in the range of the second group to be search
     * @param endDate2 First date in the range of the second group to be search
     *
     * @throws ResponseStatusException if there's any error processing the request in the service layer
     * @return List of earthquakes
     */

    @GetMapping(value="/count/countriesanddates")
    public ResponseEntity retrieveByCountryAndDates(@PathParam("country1") @Pattern(regexp = "^[A-Za-z]+$") @Valid String country1,
                                                    @PathParam("country2") @Pattern(regexp = "^[A-Za-z]+$") @Valid String country2,
                                                    @PathParam("startDate1") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String startDate1,
                                                    @PathParam("endDate1") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String endDate1,
                                                    @PathParam("startDate2") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String startDate2,
                                                    @PathParam("endDate2") @DateTimeFormat(pattern="yyyy-mm-dd") @Valid String endDate2){

        CountEarthQuakeResponse count = null;
        try {
            count = earthquakeApiService.retrieveFilterDatesAndCountry(country1, country2,
                    startDate1, endDate1, startDate2, endDate2);
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
        return new ResponseEntity<CountEarthQuakeResponse>(count, HttpStatus.OK);

    }

    public EarthquakeApiService getEarthquakeApiService() {
        return earthquakeApiService;
    }

    public void setEarthquakeApiService(EarthquakeApiService earthquakeApiService) {
        this.earthquakeApiService = earthquakeApiService;
    }
}
