package com.springboot.interview.earthquake.service;

import com.springboot.interview.earthquake.domain.CountEarthQuakeResponse;
import com.springboot.interview.earthquake.domain.EarthquakeInfo;
import com.springboot.interview.earthquake.domain.Feature;
import com.springboot.interview.earthquake.helper.EarthquakeApiHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service
public class EarthquakeApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Value( "${usgsapi.url}" )
    String url;
    /**
     * Call the USGS API to search by the given dates, divides the arguments in groups and make a call to the USGS api
     * every 2 dates
     * @param dates Dynamic dates received, this array should be > 1 and even length
     * @throws UnsupportedOperationException (Unchecked) when array < 1 and is not even length
     * @return List of earthquakes
     */

    public List<Feature> retrieveEarthquakesByDates(String ...dates){

        List<EarthquakeInfo> responseList = new ArrayList<EarthquakeInfo>();
        List<Feature> features = new ArrayList<>();

        if(dates.length%2==0 && dates.length <= 4){

           responseList =  IntStream.range(0,dates.length)
                    .filter(i -> i % 2 == 0)
                    .mapToObj(i -> retrieveEarthquakesAPI(
                            EarthquakeApiHelper.addUrlDateParams(url,dates[i],dates[i+1]))).collect(Collectors.toList());

                responseList.stream().map(x -> x.getFeatures()).forEach(x -> features.addAll(x));

               return (responseList.size() > 1) ? features.stream().distinct().collect(Collectors.toList())
                       : features;

        }else throw new UnsupportedOperationException("Incomplete dates");
    }

    /**
     * Call the USGS API to search by the given Magnitudes, if is null
     * @param minmagnitude min magnitude
     * @param  maxmagnitude max magnitude
     * @throws UnsupportedOperationException if the request wasn't processed correctly
     * @return List of earthquakes
     */

    public List<Feature>  retrieveEarthquakesByMagnitudes(String minmagnitude, String maxmagnitude){

        EarthquakeInfo earthquakeInfo = retrieveEarthquakesAPI
                (EarthquakeApiHelper.addUrlMagnitudeParams(url,minmagnitude,maxmagnitude));

        if(earthquakeInfo != null)
            return earthquakeInfo.getFeatures();
        else
            throw new UnsupportedOperationException("Error processing the given magnitudes");
    }

    /**
     * Call the USGS API implementation
     * @param url USGS url
     * @return List of earthquakes or null if the request cannot be processed
     */

    public EarthquakeInfo retrieveEarthquakesAPI(String url) {

        ResponseEntity<EarthquakeInfo> earthQuakeResponse = null;
        earthQuakeResponse = restTemplate.getForEntity(url,EarthquakeInfo.class);

        return (earthQuakeResponse != null ) ? earthQuakeResponse.getBody() : new EarthquakeInfo();
    }

    /**
     * Call the USGS API to search and then filter by the given counetry,
     * @param country country
     * @throws UnsupportedOperationException if the request wasn't processed correctly
     * @return List of earthquakes
     */
    public List<Feature> retrieveEarthquakesByCountry(String country){

        EarthquakeInfo earthquakeInfo = null;
        earthquakeInfo = retrieveEarthquakesAPI(url);
        if(earthquakeInfo != null)
            return earthquakeInfo.getFeatures().stream()
                .filter(x -> x.getProperties().getPlace().toLowerCase()
                        .contains(country.toLowerCase())).collect(Collectors.toList());
        else
            throw new UnsupportedOperationException("Error processing the given country");

    }

    /**
     * Call the USGS API to search by dates and then filter by given country,
     * @param country1 country 1
     * @param country2 country 2
     * @param dates Dynamic dates received
     * @throws UnsupportedOperationException if the request wasn't processed correctly
     * @return Count of earthquakes that met the given parameters
     */
    public CountEarthQuakeResponse retrieveFilterDatesAndCountry(String country1, String country2, String ...dates)
            throws UnsupportedOperationException{

       List<Feature> features = retrieveEarthquakesByDates(dates).stream().filter(x -> (x.getProperties().getPlace()
               .toLowerCase().contains(country1.toLowerCase()) ||
                x.getProperties().getPlace().toLowerCase()
                        .contains(country2.toLowerCase()))).collect(Collectors.toList());

       CountEarthQuakeResponse count = new CountEarthQuakeResponse();
       count.setCount(features.size());

       return count;

    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
