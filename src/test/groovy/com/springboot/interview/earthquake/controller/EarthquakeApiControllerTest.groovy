package com.springboot.interview.earthquake.controller

import com.springboot.interview.earthquake.domain.CountEarthQuakeResponse
import com.springboot.interview.earthquake.domain.EarthquakeInfo
import com.springboot.interview.earthquake.domain.Feature
import com.springboot.interview.earthquake.domain.Properties
import com.springboot.interview.earthquake.service.EarthquakeApiService
import org.mockito.InjectMocks
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.mock.env.MockEnvironment
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = [EarthquakeApiController])
class EarthquakeApiControllerTest extends Specification {

    EarthquakeInfo earthquakeInfo1,earthquakeInfo2

    @InjectMocks
    EarthquakeApiController earthquakeApiController

    MockMvc mvc

    def setup() {

        List<Feature> features1 = new ArrayList<Feature>([
                new Feature([type      : "feature",
                             properties: new Properties([place: "Mexico", mag: 6])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Spain", mag: 6])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Venezuela", mag: 7])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Chile", mag: 6.5])])
        ] as List<Feature>)

        Feature repeatedEntry = new Feature([type      : "feature",
                                             properties: new Properties([place: "Indonesia", mag: 3])]);

        List<Feature> features2 = new ArrayList<Feature>([
                new Feature([type      : "feature",
                             properties: new Properties([place: "Chile", mag: 6.5])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Japan", mag: 7])])
        ] as List<Feature>)


        earthquakeInfo1 = new EarthquakeInfo([features: features1])
        earthquakeInfo2 = new EarthquakeInfo([features: features2])

    }

    def "test retrieveByTwoDatesRange"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByDates(_) >> earthquakeInfo1.getFeatures()
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/datesrange endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/datesrange?startDate=2000-03-12&endDate=2000-10-12'))
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].properties.place").value("Mexico"))
                .andExpect(jsonPath("[1].properties.place").value("Spain"))
                .andExpect(jsonPath("[2].properties.place").value("Venezuela"))
                .andExpect(jsonPath("[3].properties.place").value("Chile"))
    }



    def "test retrieveByTwoDatesRange thrown exception an handled error is returned"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByDates(_) >> {List -> throw new UnsupportedOperationException("test message")}
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/datesrange endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/datesrange'))
                .andExpect(status().isBadRequest())
    }
    def "test retrieveByFourDatesRange "() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByDates(_) >> earthquakeInfo1.getFeatures()
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/datesrange endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/multidatesrange' +
                '?startDate1=2000-03-12&endDate1=2000-10-12&startDate2=2000-03-12&endDate2=2000-10-12'))
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].properties.place").value("Mexico"))
                .andExpect(jsonPath("[1].properties.place").value("Spain"))
                .andExpect(jsonPath("[2].properties.place").value("Venezuela"))
                .andExpect(jsonPath("[3].properties.place").value("Chile"))
    }



    def "test retrieveByFourDatesRange thrown exception an handled error is returned"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByDates(_) >> {List -> throw new UnsupportedOperationException("test message")}
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/multidatesrange endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/multidatesrange'))
                .andExpect(status().isBadRequest())
    }

    def "test retrieveByMagnitudes "() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByMagnitudes(_,_) >> earthquakeInfo1.getFeatures()
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/retrieveByMagnitudes endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/magnitudes?minMagnitude=5.5&maxMagnitude=8'))
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].properties.mag").value("6.0"))
                .andExpect(jsonPath("[1].properties.mag").value("6.0"))
                .andExpect(jsonPath("[2].properties.mag").value("7.0"))
                .andExpect(jsonPath("[3].properties.mag").value("6.5"))
    }

    def "test retrieveByMagnitudes thrown exception an handled error is returned"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByMagnitudes(_,_) >> {List -> throw new UnsupportedOperationException("test message")}
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/retrieveByMagnitudes endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/magnitudes'))
                .andExpect(status().isBadRequest())
    }

    def "test retrieveByCountry "() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByCountry(_) >> earthquakeInfo2.getFeatures()
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/country endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/country?country=chile'))
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].properties.place").value("Chile"))
    }

    def "test retrieveByCountry thrown exception an handled error is returned"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveEarthquakesByCountry(_) >> {List -> throw new UnsupportedOperationException("test message")}
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/country endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/country'))
                .andExpect(status().isBadRequest())
    }

    def "test retrieveByCountryAndDates "() {
        given: "Mocked service"
        CountEarthQuakeResponse count = new CountEarthQuakeResponse()
        count.setCount(3)
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveFilterDatesAndCountry(_,_,_,_,_,_) >> count
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/countriesanddates endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/count/countriesanddates' +
                '?startDate1=2000-03-12&endDate1=2000-10-12&startDate2=2000-03-12&endDate2=2000-10-12' +
                '&country1=chile&country2=japan'))
                .andExpect(status().isOk())
                .andExpect(jsonPath("\$.count").value("3"))
    }

    def "test retrieveByCountryAndDates thrown exception an handled error is returned"() {
        given: "Mocked service"
        EarthquakeApiService earthquakeApiService = Mock(EarthquakeApiService) {
            retrieveFilterDatesAndCountry(_,_,_,_,_,_) >> {List -> throw new UnsupportedOperationException("test message")}
        }
        mvc = MockMvcBuilders.standaloneSetup(new EarthquakeApiController([earthquakeApiService:earthquakeApiService]))
                .build();

        when: "/country endpoint is consumed"

        then:
        mvc.perform(get('/earthquakeAPI/v1/count/countriesanddates'))
                .andExpect(status().isBadRequest())
    }

}
