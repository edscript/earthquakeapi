package com.springboot.interview.earthquake.service

import com.springboot.interview.earthquake.domain.CountEarthQuakeResponse
import com.springboot.interview.earthquake.domain.EarthquakeInfo
import com.springboot.interview.earthquake.domain.Feature
import com.springboot.interview.earthquake.domain.Properties
import com.springboot.interview.earthquake.helper.EarthquakeApiHelper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Shared
import spock.lang.Specification

class EarthquakeApiServiceTest extends Specification {


    @Shared RestTemplate restTemplate
    EarthquakeInfo earthquakeInfo1,earthquakeInfo2

    @Shared def earthquakeApiHelper = Mock(EarthquakeApiHelper)



    def setup() {

        List<Feature> features1 = new ArrayList<Feature>([
                new Feature([type      : "feature",
                             properties: new Properties([place: "Mexico", code: 6])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Spain", code: 6])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Venezuela", mag: 7])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Chile", mag: 6.5])])
        ] as List<Feature>)

        Feature repeatedEntry = new Feature([type      : "feature",
                                       properties: new Properties([place: "Indonesia", mag: 3])]);

        List<Feature> features2 = new ArrayList<Feature>([
                repeatedEntry,
                new Feature([type      : "feature",
                             properties: new Properties([place: "Chile", mag: 6.5])]),
                new Feature([type      : "feature",
                             properties: new Properties([place: "Japan", mag: 7])]),
                repeatedEntry
        ] as List<Feature>)


        earthquakeInfo1 = new EarthquakeInfo([features: features1])
        earthquakeInfo2 = new EarthquakeInfo([features: features2])

    }

    def "Test method RetrieveEarthquakesByDates to send 2 dates and receive back the earthqueakes"() {
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesAPI(_) >> earthquakeInfo1
        }
        given: "2 valid dates"
        //restTemplate = Mock { getForEntity(_,_) >> new ResponseEntity(earthquakeInfo1, HttpStatus.OK)}
        //earthquakeApiService = new EarthquakeApiService([restTemplate : restTemplate])
        def (dateStart,dateEnd) = ["2000-01-01","2000-02-02"]

        when: "2 dates are passing to RetrieveEarthquakesByDates method"
        //List<Feature> feature = earthquakeApiService.retrieveEarthquakesByDates(dateStart,dateEnd)
        List<Feature> feature = earthquakeApiServiceSpy.retrieveEarthquakesByDates(dateStart,dateEnd)

        then: "retrieveEarthquakesAPI should be called"
        feature.size() == 4
        feature.get(0).getProperties().getPlace().equals("Mexico")
        feature.get(1).getProperties().getPlace().equals("Spain")
        feature.get(2).getProperties().getPlace().equals("Venezuela")
        feature.get(3).getProperties().getPlace().equals("Chile")
    }

    def "Check method retrieveEarthquakesByDates exception handle "() {
        given: "1 single date"
        EarthquakeApiService earthquakeApiService = new EarthquakeApiService();
        def (dateStart1) = ["2000-01-01","2000-01-02","2000-01-03"]

        when: "1 single date is being passing"
        List<Feature> feature = earthquakeApiService.retrieveEarthquakesByDates(dateStart1)
        and: "3 dates are passed"

        then: "Should throw an exception"
        thrown UnsupportedOperationException

    }

    def "Test magnitude method"() {
        given: "Mocked API response with 4 features"
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesAPI(_) >> earthquakeInfo1
        }
        when: "A country passing to retrieveEarthquakesByMagnitudes method"
        List<Feature> feature = earthquakeApiServiceSpy.retrieveEarthquakesByMagnitudes("1","7")

        then: "Only one result should be returned"
        feature.size() == 4
        feature.get(0).getProperties().getPlace().equals("Mexico")
        feature.get(1).getProperties().getPlace().equals("Spain")
        feature.get(2).getProperties().getPlace().equals("Venezuela")
        feature.get(3).getProperties().getPlace().equals("Chile")

    }

    def "Test retrieveEarthquakesByMagnitudes method error handling"() {
        given: "Mocked API response with 4 countries"
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesAPI(_) >> null
        }
        when: "2 magnitudes passing to retrieveEarthquakesByMagnitudes method"
        List<Feature> feature = earthquakeApiServiceSpy.retrieveEarthquakesByMagnitudes("1","7")

        then: "Only one result should be returned"
        thrown UnsupportedOperationException

    }

    def "Test method retrieveEarthquakesByCountry passing existing country"() {
        given: "Mocked API response with 4 countries"
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesAPI(_) >> earthquakeInfo2
        }
        when: "A country passing to retrieveEarthquakesByCountry method"
        List<Feature> feature = earthquakeApiServiceSpy.retrieveEarthquakesByCountry("Chile")

        then: "Only one result should be returned"
        feature.size() == 1
        feature.get(0).getProperties().getPlace().equals("Chile")

    }

    def "Test method retrieveEarthquakesByCountry error handling"() {
        given: "Mocked API null response"
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesAPI(_) >> null
        }
        when: "A country passing to retrieveEarthquakesByCountry method"
        List<Feature> feature = earthquakeApiServiceSpy.retrieveEarthquakesByCountry("Chile")

        then: "Exception is thrown"
        thrown UnsupportedOperationException

    }

    def "Test method retrieveEarthquakesAPI "() {
        given: "Mocked RestTemplate"
        restTemplate = Mock { getForEntity(_,_) >> new ResponseEntity(earthquakeInfo1, HttpStatus.OK)}
        EarthquakeApiService earthquakeApiService = new EarthquakeApiService([restTemplate:restTemplate]);

        when: "call retrieveEarthquakesAPI hitting USGS url"
        EarthquakeInfo earthquakeInfo = earthquakeApiService.retrieveEarthquakesAPI("url")

        then: "Must retrieve the given list"
        earthquakeInfo.getFeatures().size() == 4
        earthquakeInfo.getFeatures().get(0).getProperties().getPlace().equals("Mexico")
        earthquakeInfo.getFeatures().get(1).getProperties().getPlace().equals("Spain")
        earthquakeInfo.getFeatures().get(2).getProperties().getPlace().equals("Venezuela")
        earthquakeInfo.getFeatures().get(3).getProperties().getPlace().equals("Chile")
    }


    def "Test method retrieveFilterDatesAndCountry passing existing country"() {
        given: "Mocked API response with 4 countries"
        def earthquakeApiServiceSpy = Spy(EarthquakeApiService){
            retrieveEarthquakesByDates(_) >> earthquakeInfo1.getFeatures()
        }
        when: "A country and dates passing to retrieveFilterDatesAndCountry method"
        CountEarthQuakeResponse count = earthquakeApiServiceSpy.retrieveFilterDatesAndCountry("Japan","Chile",
        "2000-10-03","2000-10-04","2000-10-05","2000-10-06")

        then: "Only one result should be returned"
        count.getCount() == 1

    }

}
