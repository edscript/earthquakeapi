# earthquakeAPI

What This Is
------------

API knowledge test for hiring process on IT-Talent, the purpuse of this API is get earthquakes 
and filtering the data retrieved from USGS API.

To Run
---------

    git clone https://edscript@bitbucket.org/edscript/earthquakeapi.git
    cd earthquakeapi
    gradle build
    java -jar build/libs/earthquakeAPI-0.0.1-SNAPSHOT.jar


**Retrive Earthquakes by date**
----
  Retrive feature information form the earthquakes using USGS 

***URL**

  _/earthquakeAPI/v1/datesrange?startDate=2000-03-12&endDate=2000-04-01_

***Method:**

  `GET`
  
***URL Params**

   **Required:**
 
   startDate`=[String] "yyyy-mm-dd"` 
   endDate`=[String] "yyyy-mm-dd"`

* **Success Response:**
  
  ***Code:** 200
    **Content:** `[
    {
        "type": "Feature",
        "properties": {
            "mag": 5.2,
            "place": "West Chile Rise",
            "time": 1574513441001,
            "updated": 1574515634040,
            "tz": -300,
            "url": "https://earthquake.usgs.gov/earthquakes/eventpage/us70006c7a",
            "detail": "https://earthquake.usgs.gov/fdsnws/event/1/query?eventid=us70006c7a&format=geojson",
            "felt": null,
            "cdi": null,
            "mmi": null,
            "alert": null,
            "status": "reviewed",
            "tsunami": 0,
            "sig": 416,
            "net": "us",
            "code": "70006c7a",
            "ids": ",us70006c7a,",
            "sources": ",us,",
            "types": ",geoserve,moment-tensor,origin,phase-data,",
            "nst": null,
            "dmin": 6.105,
            "rms": 1.4,
            "gap": 73.0,
            "maqType": null,
            "string": null
        }
    },...`
 
* **Error Response:**

  If the dates are not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Incomplete dates",
    "path": "/earthquakeAPI/v1/datesrange"
}`


**Retrive Earthquakes by dates**
----
  Retrive feature information form the earthquakes using USGS using multiples dates, if the ranges are overlapping 
  and repeated features are retrieve the API will return just distinct elements by eliminate the repeated entries
  from the response

***URL**

  _/earthquakeAPI/v1/datesrange?startDate1=2000-03-12&endDate1=2000-04-01&startDate1=2000-03-12&endDate1=2000-04-01_

***Method:**

  `GET`
  
***URL Params**

   **Required:**
 
   startDate1`=[String] "yyyy-mm-dd"` 
   endDate1`=[String] "yyyy-mm-dd"`
   startDate2`=[String] "yyyy-mm-dd"` 
   endDate2`=[String] "yyyy-mm-dd"`

* **Success Response:**
  
  ***Code:** 200
    **Content:** `[
    {
        "type": "Feature",
        "properties": {
            "mag": 5.2,
            "place": "West Chile Rise",
            "time": 1574513441001,
            "updated": 1574515634040,
            "tz": -300,
            "url": "https://earthquake.usgs.gov/earthquakes/eventpage/us70006c7a",
            "detail": "https://earthquake.usgs.gov/fdsnws/event/1/query?eventid=us70006c7a&format=geojson",
            "felt": null,
            "cdi": null,
            "mmi": null,
            "alert": null,
            "status": "reviewed",
            "tsunami": 0,
            "sig": 416,
            "net": "us",
            "code": "70006c7a",
            "ids": ",us70006c7a,",
            "sources": ",us,",
            "types": ",geoserve,moment-tensor,origin,phase-data,",
            "nst": null,
            "dmin": 6.105,
            "rms": 1.4,
            "gap": 73.0,
            "maqType": null,
            "string": null
        }
    },...`
 
* **Error Response:**

  If the dates are not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Incomplete dates",
    "path": "/earthquakeAPI/v1/datesrange"
}`



**Retrive Earthquakes by magnitudes**
----
  Retrive feature information form the earthquakes using USGS 

***URL**

  _/earthquakeAPI/v1/magnitudes?minMagnitude=1.1&maxMagnitude=6_

***Method:**

  `GET`
  
***URL Params**

   **Required:**
 
   minMagnitude`=[Decimal]` 
   minMagnitude`=[Decimal]`

* **Success Response:**
  
  ***Code:** 200
    **Content:** `[
    {
        "type": "Feature",
        "properties": {
            "mag": 5.2,
            "place": "West Chile Rise",
            "time": 1574513441001,
            "updated": 1574515634040,
            "tz": -300,
            "url": "https://earthquake.usgs.gov/earthquakes/eventpage/us70006c7a",
            "detail": "https://earthquake.usgs.gov/fdsnws/event/1/query?eventid=us70006c7a&format=geojson",
            "felt": null,
            "cdi": null,
            "mmi": null,
            "alert": null,
            "status": "reviewed",
            "tsunami": 0,
            "sig": 416,
            "net": "us",
            "code": "70006c7a",
            "ids": ",us70006c7a,",
            "sources": ",us,",
            "types": ",geoserve,moment-tensor,origin,phase-data,",
            "nst": null,
            "dmin": 6.105,
            "rms": 1.4,
            "gap": 73.0,
            "maqType": null,
            "string": null
        }
    },...`
 
* **Error Response:**

  If the magnitude is not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Invalid magnitudes",
    "path": "/earthquakeAPI/v1/datesrange"
}`



**Retrive Earthquakes by Country**
----
  Retrive feature information form the earthquakes using USGS, since the USGS doesn't have any direct way
   to filter the country, a default search is performed in USGS API and then is filtered by this API

***URL**

  _/earthquakeAPI/v1/country?country=chile

***Method:**

  `GET`
  
***URL Params**

   **Required:**
 
   country`=[String]` 


* **Success Response:**
  
  ***Code:** 200
    **Content:** `[
    {
        "type": "Feature",
        "properties": {
            "mag": 5.2,
            "place": "West Chile",
            "time": 1574513441001,
            "updated": 1574515634040,
            "tz": -300,
            "url": "https://earthquake.usgs.gov/earthquakes/eventpage/us70006c7a",
            "detail": "https://earthquake.usgs.gov/fdsnws/event/1/query?eventid=us70006c7a&format=geojson",
            "felt": null,
            "cdi": null,
            "mmi": null,
            "alert": null,
            "status": "reviewed",
            "tsunami": 0,
            "sig": 416,
            "net": "us",
            "code": "70006c7a",
            "ids": ",us70006c7a,",
            "sources": ",us,",
            "types": ",geoserve,moment-tensor,origin,phase-data,",
            "nst": null,
            "dmin": 6.105,
            "rms": 1.4,
            "gap": 73.0,
            "maqType": null,
            "string": null
        }
    },...`
 
* **Error Response:**

  If the country is not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Error processing the given country",
    "path": "/earthquakeAPI/v1/datesrange"
}`


**Count Earthquakes by dates and country**
----
  Count feature information form the earthquakes using USGS using multiples dates, if the ranges are overlapping 
  and repeated features are retrieve the API will return just distinct elements by eliminate the repeated entries
  from the response, since the USGS doesn't have any direct way to filter the country, a default search is performed 
  in USGS API and then is filtered by this API

***URL**

  _/earthquakeAPI/v1/datesrange?startDate1=2000-03-12&endDate1=2000-04-01&startDate1=2000-03-12&endDate1=2000-04-01_

***Method:**

  `GET`
  
***URL Params**

   **Required:**
   
   startDate1`=[String] "yyyy-mm-dd"` 
   startDate2`=[String] "yyyy-mm-dd"` 
   startDate1`=[String] "yyyy-mm-dd"` 
   endDate1`=[String] "yyyy-mm-dd"`
   startDate2`=[String] "yyyy-mm-dd"` 
   endDate2`=[String] "yyyy-mm-dd"`

* **Success Response:**
  
  ***Code:** 200
    **Content:** `{
    "count": 176
}`
 
* **Error Response:**

  If the dates are not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Incomplete dates",
    "path": "/earthquakeAPI/v1/datesrange"
}`

  If the country is not correct it will return the following error:
  
  ***Code:** 400 BAD REQUEST 
    **Content:** `{
    "timestamp": "2019-11-24T20:47:13.095+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Incomplete dates",
    "path": "/earthquakeAPI/v1/datesrange"
}`

